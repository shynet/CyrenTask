from __future__ import unicode_literals

from django.db import models

USER_STATUS = (
    (0, 'Not set'),
    (1, 'Working'),
    (2, 'On Vacation'),
    (3, 'Business Trip'),
)

class User(models.Model):
    """
    A simple user model containing information about a user.
    We are not using Django default user model as this model is more simplified.
    """
    username = models.CharField(max_length=80)
    status = models.IntegerField(default=0)

    @property
    def status_name(self):
        return (USER_STATUS[self.status])[1]

    def update_status(self, new_status):
        """
        Update user status according to the provided status name if exists.
        :param new_status: 
        :return: 
        """
        for status in USER_STATUS:
            status_name = status[1]

            if status_name.lower() == new_status.lower():
                self.status = status[0]
                return

        raise Exception("The specified status is not valid!")
