class CorsMiddleware(object):
    """
    This middleware is responsible of adding Cross-Origin Resource Sharing to allow 3rd parties
    to access the api.
    """

    def process_response(self, request, response):
        response['Access-Control-Allow-Origin'] = '*'
        # response['Access-Control-Allow-Credentials'] = 'true'
        return response