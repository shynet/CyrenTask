from core.models import User
from core.serializers import UserSerializer
from models import ActionResponse
from serializers import ActionResponseSerializer
from decorators import json_response

@json_response(ActionResponseSerializer)
def login_or_register(request):
    """
    Logins or registers a user if it does not exist.
    Empty username will result in an error.
    :param request: 
    :return: 
    """
    username = request.GET.get('username')

    if not username:
        response = ActionResponse(status='error', error='Invalid username!')
    else:
        user = None

        try:
            user = User.objects.get(username=username)
            response = ActionResponse(status='ok')
        except User.DoesNotExist:
            user = User(username=username)
            user.save()
            response = ActionResponse(status='ok')

        response.data = { 'profile' : UserSerializer(user).data }

    return response

@json_response(ActionResponseSerializer)
def update_status(request):
    """
    Updates the user status according to the provided params.
    If an invalid status name was provided, an error will be thrown and returned.
    :param request: 
    :return: 
    """
    new_status = request.GET.get('CurrentStatus')
    unique_id = request.GET.get('UniqueID')

    try:
        user = User.objects.get(pk=unique_id)
        user.update_status(new_status)
        user.save()

        response = ActionResponse(status='ok')

    except User.DoesNotExist:
        response = ActionResponse(status='error', error='The specified user id does not exist!')
    except Exception as ex:
        response = ActionResponse(status='error', error='%s' % ex)

    return response

@json_response(UserSerializer, True)
def get_users(request):
    """
    Returns all of the users and their status codes.
    :param request: 
    :return: 
    """

    users = User.objects.all()
    return users