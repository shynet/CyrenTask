from core.responses import JSONResponse
from api.models import ActionResponse

def json_response(serializer, many=False):
    """
    Returns a json response using the specified serializer. If the response is ActionResponse and
    contains an error, a bad request http status code will be set.
    """

    def json_decorator(func):
        def wrapper_view(*args, **kwargs):
            response = func(*args, **kwargs)

            # The json data obtained from the provided serializer class
            response_data = serializer(response,many=many).data

            # By default all responses return status ok
            status = 200

            # This is an action response and contains an error, return status 400
            if type(response) is ActionResponse and response.status == 'error':
                status = 400

            return JSONResponse(response_data, status=status)

        return wrapper_view

    return json_decorator