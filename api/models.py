from __future__ import unicode_literals

from django.db import models

class ActionResponse(models.Model):
    """
    Returns the response for an action.
    """

    status = models.CharField(max_length=100)
    error = models.CharField(max_length=400)
    data = None

    class Meta:
        managed = False
