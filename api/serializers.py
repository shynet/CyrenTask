from rest_framework import serializers
from models import *

class ActionResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActionResponse
        fields = ('status', 'error', 'data')