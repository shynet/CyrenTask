from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^login_or_register/$', 'api.views.login_or_register'),
    url(r'^update_status/$', 'api.views.update_status'),
    url(r'^users/$', 'api.views.get_users'),
)