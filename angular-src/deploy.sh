# To use this script, on windows open up git bash and cd into the angular-src directory, then type ./deploy.sh
# When script finished the deployment you can open up http://localhost:8000
echo "Removing dist directory..."
rm -rf ./dist
echo "Removing assets directory..."
rm -rf ../assets
echo "Building prod files..."
ng build -prod
echo "Copying files to appropriate locations..."
cp dist/index.html ../web/templates/index.html
mkdir ../assets
cp -R ./dist/* ../assets/
cp -R ./dist/assets/* ../assets/
rm -rf ../assets/index.html
rm -rf ../assets/assets
rm -rf dist
cd ../web/templates
echo "Replacing static paths in index.html..."
sed -i -- 's/src=\"/src=\"assets\//g' *
sed -i -- 's/link href=\"/link href=\"assets\//g' *
echo "Deployment finished! Running django..."
cd ../../
python manage.py runserver 0.0.0.0:8000