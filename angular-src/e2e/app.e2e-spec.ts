import { MyWorkStatusPage } from './app.po';

describe('my-work-status App', () => {
  let page: MyWorkStatusPage;

  beforeEach(() => {
    page = new MyWorkStatusPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
