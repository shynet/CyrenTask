import { Pipe, PipeTransform } from '@angular/core';

import { User } from '../models/User';

// A simple filter pass an array of user with a certain status
@Pipe({name: 'statusFilter'})
export class StatusFilterPipe implements PipeTransform {
    transform(items: User[], status: string): any {
        if (status) status = status.toLocaleLowerCase();
        return items.filter(item => !status || item.status_name.toLowerCase().indexOf(status) != -1);
    }
}