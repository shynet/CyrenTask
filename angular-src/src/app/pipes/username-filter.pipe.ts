import { Pipe, PipeTransform } from '@angular/core';

import { User } from '../models/User';

// A simple filter to pass only users that contain the specified username string within
@Pipe({name: 'usernameFilter'})
export class UsernameFilterPipe implements PipeTransform {
    transform(items: User[], username: string): any {
        if (username) username = username.toLocaleLowerCase();
        return items.filter(item => !username || item.username.toLowerCase().indexOf(username) != -1);
    }
}