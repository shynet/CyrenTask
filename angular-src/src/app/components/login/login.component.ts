import { Component, OnInit } from '@angular/core';

import { AppService, ApiService } from '../../services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  username:string = "";

  constructor(public appService:AppService, private apiService:ApiService) { }

  ngOnInit() {}

  onLoginClick() {
    if (!this.username) { 
      alert("Username is empty!");
      return;
    }

    this.appService.isLoading = true;

    // Sends an AJAX request to sign in or register with the provided information
    this.apiService.loginOrRegister(this.username).then(response => {
      this.appService.user = response.json().data.profile;
      this.appService.isLoading = false;
    })
    .catch(error => {
      this.appService.isLoading = false;
      alert(`There was an error: ${error}`);
    });
  }
}
