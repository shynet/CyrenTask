import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusUpdaterComponent } from './status-updater.component';

describe('StatusUpdaterComponent', () => {
  let component: StatusUpdaterComponent;
  let fixture: ComponentFixture<StatusUpdaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusUpdaterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusUpdaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
