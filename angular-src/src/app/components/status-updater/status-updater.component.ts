import { Component, OnInit } from '@angular/core';
import { AppService, ApiService } from '../../services';

// A simple dropdown to update our current working status
@Component({
  selector: 'app-status-updater',
  templateUrl: './status-updater.component.html',
  styleUrls: ['./status-updater.component.css']
})
export class StatusUpdaterComponent implements OnInit {

  constructor(private appService:AppService, 
    private apiService:ApiService) { }

  ngOnInit() {
  }

  onStatusChange(newStatus) {
    this.appService.isLoading = true;
    this.apiService.updateStatus(this.appService.user.id, newStatus)
    .then(response => {
      this.appService.user.status_name = newStatus;
      this.appService.isLoading = false;

      // Update the fetched users array with the new status of our user
      if (this.appService.fetchedUsers) {
        for (let user of this.appService.fetchedUsers) {
          if (user.id == this.appService.user.id) {
            user.status_name = newStatus;
            break;
          }
        }
      }
    }).catch(error => {
      this.appService.isLoading = false;
      alert(`Failed to update: ${error}`);
    });
  }
}
