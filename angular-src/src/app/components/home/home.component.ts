import { Component, OnInit } from '@angular/core';
import { AppService } from '../../services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public appService:AppService) { }

  ngOnInit() {
  }

}
