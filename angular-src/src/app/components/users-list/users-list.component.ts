import { Component, OnInit } from '@angular/core';
import { AppService, ApiService } from '../../services';
import { User } from '../../models/User';

// The main list which contains all of the users with the option of filterting them easily.
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  searchedUsername:string = "";
  searchedStatus = "";

  constructor(public appService:AppService, private apiService:ApiService) { }

  ngOnInit() {
    this.apiService.fetchUsers().subscribe(results => {
      this.appService.fetchedUsers = results;
    });
  }
}
