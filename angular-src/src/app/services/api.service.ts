import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

// This service provides an easy to use gateway to our API.
@Injectable()
export class ApiService {
    constructor(private httpService: Http) {

    }

    domain = "http://localhost:8000";
    get apiUrl():string { return `${this.domain}/api`; }
    getApiEndpoint(endpoint):string { return `${this.apiUrl}/${endpoint}`; }

    loginOrRegister(username) {
        let url = this.getApiEndpoint(`login_or_register/?username=${username}`);
        return this.httpService.get(url).toPromise();
    }

    updateStatus(userId, newStatus) {
        let url = this.getApiEndpoint(`update_status/?UniqueID=${userId}&CurrentStatus=${newStatus}`);
        return this.httpService.get(url).toPromise();
    }

    fetchUsers() {
        let url = this.getApiEndpoint(`users/`);
        return this.httpService.get(url).map(res => res.json());
    }
}