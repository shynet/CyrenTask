import { Injectable } from '@angular/core';

import { User } from '../models/User';

// The app service contains the main core of our app - it holds important information such as the currently logged user and other required information.
@Injectable()
export class AppService {
    public isLoading = false;
    public get isLoggedIn() { return this.user != null; }
    public user:User;
    public fetchedUsers:User[];

    constructor() {

    }
}