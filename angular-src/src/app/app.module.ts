import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppService, ApiService } from './services';
import { StatusFilterPipe, UsernameFilterPipe } from './pipes';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { StatusUpdaterComponent } from './components/status-updater/status-updater.component';
import { UsersListComponent } from './components/users-list/users-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    StatusUpdaterComponent,
    UsersListComponent,
    UsernameFilterPipe,
    StatusFilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    AppService,
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
